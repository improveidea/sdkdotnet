﻿using System;
using System.Net;
using System.IO;
using Newtonsoft.Json;
using Newtonsoft.Json.Linq;
using System.Text;

namespace RequisicoesWeb
{
    class Program
    {
        static void Main(string[] args)
        {
            //EnviaRequisicaoGET();
            EnviaRequisicaoPOST();           
        }

        public static void EnviaRequisicaoGET(Token token)
        {
            var requisicaoWeb = WebRequest.CreateHttp("http://localhost/store/api-loja/public/categories");
            requisicaoWeb.Method = "GET";
            requisicaoWeb.UserAgent = "RequisicaoWebDemo";
            requisicaoWeb.Headers.Add(HttpRequestHeader.Authorization, "Bearer " + token.access_token);

            using (var resposta = requisicaoWeb.GetResponse())
            {
                var streamDados = resposta.GetResponseStream();
                StreamReader reader = new StreamReader(streamDados);
                object objResponse = reader.ReadToEnd();

                var data = JObject.Parse(objResponse.ToString());

                Console.WriteLine((string)data.SelectToken("data[0].name"));
                Console.WriteLine(data);
                Console.ReadLine();
                streamDados.Close();
                resposta.Close();
            }
            //Console.ReadLine();
        }

        public static void EnviaRequisicaoPOST()
        {
            string dadosPOST = "grant_type=client_credentials";
            dadosPOST = dadosPOST + "&client_id=1";
            dadosPOST = dadosPOST + "&client_secret=SSg7tjCg5a44drhZuQeN5jZePHxI204DMVCNo53T&scope=*";

            var dados = Encoding.UTF8.GetBytes(dadosPOST);

            var requisicaoWeb = WebRequest.CreateHttp("http://localhost/store/api-loja/public/oauth/token");

            requisicaoWeb.Method = "POST";
            requisicaoWeb.ContentType = "application/x-www-form-urlencoded";
            requisicaoWeb.ContentLength = dados.Length;
            
            //precisamos escrever os dados post para o stream
            using (var stream = requisicaoWeb.GetRequestStream())
            {
                stream.Write(dados, 0, dados.Length);
                stream.Close();
            }

            using (var resposta = requisicaoWeb.GetResponse())
            {
                var streamDados = resposta.GetResponseStream();
                StreamReader reader = new StreamReader(streamDados);
                object objResponse = reader.ReadToEnd();

                var token = JsonConvert.DeserializeObject<Token>(objResponse.ToString());
                EnviaRequisicaoGET(token);
                streamDados.Close();
                resposta.Close();
            }
            
            Console.ReadLine();
        }
    }
}
